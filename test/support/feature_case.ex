defmodule EmailPasswordWeb.FeatureCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      use Wallaby.DSL

      alias EmailPassword.Repo
      import Ecto
      import Ecto.Changeset
      import Ecto.Query

      import EmailPasswordWeb.Router.Helpers
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(EmailPassword.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(EmailPassword.Repo, {:shared, self()})
    end

    metadata = Phoenix.Ecto.SQL.Sandbox.metadata_for(EmailPassword.Repo, self())

    {:ok, session} =
      Wallaby.start_session(metadata: metadata, window_size: [width: 1280, height: 720])

    {:ok, session: session}
  end
end
