defmodule EmailPasswordWeb.UserView do
  use EmailPasswordWeb, :view

  def avatar_url(user, version \\ :thumb) do
    EmailPassword.User.Avatar.url({user.avatar, user}, version, signed: true)
  end
end
