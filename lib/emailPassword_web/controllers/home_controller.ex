defmodule EmailPasswordWeb.HomeController do
  use EmailPasswordWeb, :controller

  def index(conn, _params) do
    conn |> render()
  end
end
