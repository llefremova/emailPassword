use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :emailPassword, EmailPasswordWeb.Endpoint,
  http: [port: 4002],
  server: true

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :emailPassword, EmailPassword.Repo,
  username: "postgres",
  password: "postgres",
  database: "emailPassword_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :emailPassword, :sql_sandbox, true

config :wallaby, driver: Wallaby.Experimental.Chrome
